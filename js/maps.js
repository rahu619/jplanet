      function initMap() {
        var uluru = {
          lat: -37,
          lng: 174.17
        };
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 8,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          icon: 'images/map-marker.png'
        });
      }